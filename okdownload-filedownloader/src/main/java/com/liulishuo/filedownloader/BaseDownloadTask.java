/*
 * Copyright (c) 2018 LingoChamp Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.liulishuo.filedownloader;


import android.support.annotation.Nullable;

import com.liulishuo.filedownloader.model.FileDownloadStatus;

/**
 * An atom download task.
 *
 * @see FileDownloader
 */
@SuppressWarnings({"WeakerAccess", "UnusedReturnValue"})
public interface BaseDownloadTask {

    int DEFAULT_CALLBACK_PROGRESS_MIN_INTERVAL_MILLIS = 10;

    BaseDownloadTask setMinIntervalUpdateSpeed(int minIntervalUpdateSpeedMs);

    BaseDownloadTask setPath(String path);

    BaseDownloadTask setPath(String path, boolean pathAsDirectory);

    BaseDownloadTask setListener(FileDownloadListener listener);

    BaseDownloadTask setCallbackProgressTimes(int callbackProgressCount);

    BaseDownloadTask setCallbackProgressMinInterval(int minIntervalMillis);

    BaseDownloadTask setCallbackProgressIgnored();

    BaseDownloadTask setTag(Object tag);

    BaseDownloadTask setTag(int key, Object tag);

    BaseDownloadTask setForceReDownload(boolean isForceReDownload);

    BaseDownloadTask setFinishListener(FinishListener finishListener);

    BaseDownloadTask addFinishListener(FinishListener finishListener);

    boolean removeFinishListener(FinishListener finishListener);

    BaseDownloadTask setAutoRetryTimes(int autoRetryTimes);

    BaseDownloadTask addHeader(String name, String value);

    BaseDownloadTask addHeader(String line);

    BaseDownloadTask removeAllHeaders(String name);

    BaseDownloadTask setSyncCallback(boolean syncCallback);

    BaseDownloadTask setWifiRequired(boolean isWifiRequired);

    @Deprecated
    int ready();

    InQueueTask asInQueueTask();

    boolean reuse();

    boolean isUsing();

    boolean isRunning();

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    boolean isAttached();

    int start();

    boolean pause();

    boolean cancel();

    int getId();

    int getDownloadId();

    String getUrl();

    int getCallbackProgressTimes();

    int getCallbackProgressMinInterval();

    String getPath();

    boolean isPathAsDirectory();

    String getFilename();

    String getTargetFilePath();

    FileDownloadListener getListener();

    int getSoFarBytes();

    int getSmallFileSoFarBytes();

    long getLargeFileSoFarBytes();

    int getTotalBytes();

    int getSmallFileTotalBytes();

    long getLargeFileTotalBytes();

    int getSpeed();

    byte getStatus();

    boolean isForceReDownload();

    Throwable getEx();

    Throwable getErrorCause();

    boolean isReusedOldFile();

    Object getTag();

    Object getTag(int key);

    boolean isContinue();

    boolean isResuming();

    String getEtag();

    int getAutoRetryTimes();

    int getRetryingTimes();

    boolean isSyncCallback();

    boolean isLargeFile();

    boolean isWifiRequired();

    interface InQueueTask {
        int enqueue();
    }

    @SuppressWarnings("UnusedParameters")
    interface FinishListener {
        void over(BaseDownloadTask task);
    }

    interface IRunningTask {

        BaseDownloadTask getOrigin();

        ITaskHunter.IMessageHandler getMessageHandler();

        boolean is(int id);

        boolean is(FileDownloadListener listener);

        @SuppressWarnings("BooleanMethodIsAlwaysInverted")
        boolean isOver();

        int getAttachKey();

        void setAttachKeyByQueue(int key);

        void setAttachKeyDefault();

        boolean isMarkedAdded2List();

        void markAdded2List();

        void free();

        void startTaskByQueue();

        void startTaskByRescue();

        @Nullable
        Object getPauseLock();

        boolean isContainFinishListener();
    }

    interface LifeCycleCallback {

        void onBegin();

        void onIng();

        void onOver();
    }
}
