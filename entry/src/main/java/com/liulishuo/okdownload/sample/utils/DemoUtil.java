/*
 * Copyright (c) 2017 LingoChamp Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.liulishuo.okdownload.sample.utils;

import ohos.agp.components.Slider;
import ohos.app.Context;

import java.io.File;

/**
 * DemoUtil
 */
public class DemoUtil {
    /**
     * calcProgressToView
     *
     * @param progressBar 进度条
     * @param offset 当前
     * @param total 总共
     */
    public static void calcProgressToView(Slider progressBar, long offset, long total) {
        final float percent = (float) offset / total;
        progressBar.setProgressValue((int) (percent * progressBar.getMax()));
    }

    /**
     * getParentFile
     *
     * @param context 上下文
     * @return File
     */
    public static File getParentFile(Context context) {
        return context.getCacheDir();
    }
}
