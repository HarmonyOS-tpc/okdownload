/*
 * Copyright (c) 2018 LingoChamp Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.liulishuo.okdownload.sample;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Button;

/**
 * NavigationAbility
 */
public class NavigationAbility extends Ability {
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_navigation_layout);
        Button singleDownload = (Button) findComponentById(ResourceTable.Id_navigation1);
        Button queueDownload = (Button) findComponentById(ResourceTable.Id_navigation2);

        singleDownload.setClickedListener(component -> goToDownloadAbility("com.liulishuo.okdownload.sample.MainAbility"));
        queueDownload.setClickedListener(component -> goToDownloadAbility("com.liulishuo.okdownload.sample.EnqueueAbility"));
    }

    private void goToDownloadAbility(String abilityName) {
        Intent intent = new Intent();
        // 通过Intent 中的 OperationBuilder 类构造 operation 对象，指定设备标识（空串表示当前设备）、应用包名、Ability 名称
        Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName("com.liulishuo.sample")
                .withAbilityName(abilityName)
                .build();
        // 把operation 设置到 intent 中
        intent.setOperation(operation);
        startAbility(intent);
    }
}
