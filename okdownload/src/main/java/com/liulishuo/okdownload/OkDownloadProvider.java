/*
 * Copyright (c) 2017 LingoChamp Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.liulishuo.okdownload;

import com.liulishuo.okdownload.core.LogUtillib;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.app.Context;
import ohos.data.dataability.DataAbilityPredicates;
import ohos.data.rdb.ValuesBucket;
import ohos.data.resultset.ResultSet;
import ohos.utils.net.Uri;

/**
 * 原框架是通过配置ContentProvider的方式来将entry中的context传到okdownload模块中，
 * OkDownload with（new Builder(OkDownloadProvider.context)）会使用。
 * 但是查询文档发现：Ability的type配置为data后，
 * <p>
 * 替代方案： 变成服务来传递context
 */
public class OkDownloadProvider extends Ability {
    public static Context context;

    @Override
    protected void onStart(Intent intent) {
        context = getContext();
        LogUtillib.info("zzhs", "OkDownloadProvider onStart:" + context);
    }

    @Override
    public ResultSet query(Uri uri, String[] columns, DataAbilityPredicates predicates) {
        return null;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public int insert(Uri uri, ValuesBucket value) {
        return 0;
    }

    @Override
    public int delete(Uri uri, DataAbilityPredicates predicates) {
        return 0;
    }

    @Override
    public int update(Uri uri, ValuesBucket value, DataAbilityPredicates predicates) {
        return 0;
    }

}
