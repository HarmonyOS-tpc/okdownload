package com.liulishuo.okdownload.core;

import ohos.app.Context;

public class ContextHelper {

    private static Context APP_CONTEXT;

    public static void holdContext(final Context context) {
        APP_CONTEXT = context;
    }

    public static Context getAppContext() {
        return APP_CONTEXT;
    }
}
