package com.liulishuo.okdownload.core.breakpoint;

import java.util.ArrayList;
import java.util.List;

/**
 * 类型转换工具类
 */
public class ObjCastUtils {

    public static <T> List<T> objToList(Object obj, Class<T> cla) throws ClassCastException {
        List<T> list = new ArrayList<T>();
        if (obj instanceof ArrayList<?>) {
            for (Object o : (List<?>) obj) {
                list.add(cla.cast(o));
            }
            return list;
        }
        return null;
    }
}
