/*
 * Copyright (c) 2017 LingoChamp Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.liulishuo.okdownload.core.breakpoint;

import android.support.annotation.NonNull;
import com.liulishuo.okdownload.core.exception.SQLiteException;
import ohos.app.Context;
import ohos.data.DatabaseHelper;
import ohos.data.rdb.*;
import ohos.data.resultset.ResultSet;
import ohos.utils.PlainArray;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import static com.liulishuo.okdownload.core.breakpoint.BreakpointSQLiteKey.*;

public class BreakpointSQLiteHelper extends DatabaseHelper {

    private static final String NAME = "okdownload-breakpoint.db";
    private static final int VERSION = 3;

    private static final String RESPONSE_FILENAME_TABLE_NAME = "okdownloadResponseFilename";
    private static final String BREAKPOINT_TABLE_NAME = "breakpoint";
    private static final String BLOCK_TABLE_NAME = "block";
    static final String TASK_FILE_DIRTY_TABLE_NAME = "taskFileDirty";
    RdbStore rdbStore;

    public BreakpointSQLiteHelper(Context context) {
        super(context);
        getWritableDatabase();
    }

    /**
     * 创建数据库
     *
     * @return RdbStore
     */
    public RdbStore getWritableDatabase() {
        if (rdbStore != null && rdbStore.isOpen()) {
            return rdbStore;
        }
        synchronized (this) {
            StoreConfig config = StoreConfig.newDefaultConfig(NAME);
            RdbOpenCallback rdbOpenCallback = new RdbOpenCallback() {
                @Override
                public void onCreate(RdbStore rdbStore) {
                    rdbStore.executeSql("CREATE TABLE IF NOT EXISTS "
                            + BREAKPOINT_TABLE_NAME + "( "
                            + ID + " INTEGER PRIMARY KEY, "
                            + URL + " VARCHAR NOT NULL, "
                            + ETAG + " VARCHAR, "
                            + PARENT_PATH + " VARCHAR NOT NULL, "
                            + FILENAME + " VARCHAR, "
                            + TASK_ONLY_PARENT_PATH + " TINYINT(1) DEFAULT 0, "
                            + CHUNKED + " TINYINT(1) DEFAULT 0)"
                    );

                    rdbStore.executeSql("CREATE TABLE IF NOT EXISTS "
                            + BLOCK_TABLE_NAME + "( "
                            + ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                            + HOST_ID + " INTEGER, "
                            + BLOCK_INDEX + " INTEGER, "
                            + START_OFFSET + " INTEGER, "
                            + CONTENT_LENGTH + " INTEGER, "
                            + CURRENT_OFFSET + " INTEGER)"
                    );

                    rdbStore.executeSql("CREATE TABLE IF NOT EXISTS "
                            + RESPONSE_FILENAME_TABLE_NAME + "( "
                            + URL + " VARCHAR NOT NULL PRIMARY KEY, "
                            + FILENAME + " VARCHAR NOT NULL)"
                    );

                    rdbStore.executeSql("CREATE TABLE IF NOT EXISTS "
                            + TASK_FILE_DIRTY_TABLE_NAME + "( "
                            + ID + " INTEGER PRIMARY KEY)");
                }

                @Override
                public void onUpgrade(RdbStore rdbStore, int oldVersion, int newVersion) {
                    if (oldVersion == 1 && newVersion == 2) {
                        rdbStore.executeSql("CREATE TABLE IF NOT EXISTS "
                                + RESPONSE_FILENAME_TABLE_NAME + "( "
                                + URL + " VARCHAR NOT NULL PRIMARY KEY, "
                                + FILENAME + " VARCHAR NOT NULL)"
                        );
                    }

                    if (oldVersion <= 2) {
                        rdbStore.executeSql("CREATE TABLE IF NOT EXISTS "
                                + TASK_FILE_DIRTY_TABLE_NAME + "( "
                                + ID + " INTEGER PRIMARY KEY)");
                    }
                    onCreate(rdbStore);
                }

                @Override
                public void onDowngrade(RdbStore store, int currentVersion, int targetVersion) {
                    onUpgrade(store,currentVersion,targetVersion);
                }
            };
            rdbStore = getRdbStore(config, VERSION, rdbOpenCallback);
            return rdbStore;
        }
    }


    public void close() {
        if (rdbStore!=null){
            rdbStore.close();
        }
    }

    public void markFileDirty(int id) {
        final RdbStore db = getWritableDatabase();
        ValuesBucket values = new ValuesBucket(1);
        values.putInteger(ID, id);
        db.insert(TASK_FILE_DIRTY_TABLE_NAME, values);
    }

    public void markFileClear(int id) {
        RawRdbPredicates rawRdbPredicates = new RawRdbPredicates(TASK_FILE_DIRTY_TABLE_NAME, ID + " = ?",
                new String[]{String.valueOf(id)});
        getWritableDatabase().delete(rawRdbPredicates);
    }

    public List<Integer> loadDirtyFileList() {
        final List<Integer> dirtyFileList = new ArrayList<>();
        ResultSet cursor = null;
        try {
            cursor = getWritableDatabase().querySql("SELECT * FROM " + TASK_FILE_DIRTY_TABLE_NAME,
                    null);
            while (cursor.goToNextRow()) {
                dirtyFileList.add(cursor.getInt(cursor.getColumnIndexForName(ID)));
            }
        } finally {
            if (cursor != null) cursor.close();
        }

        return dirtyFileList;
    }

    public PlainArray<BreakpointInfo> loadToCache() {
        ResultSet breakpointCursor = null;
        ResultSet blockCursor = null;
        final RdbStore db = getWritableDatabase();

        final List<BreakpointInfoRow> breakpointInfoRows = new ArrayList<>();
        final List<BlockInfoRow> blockInfoRows = new ArrayList<>();

        try {
            breakpointCursor = db.querySql("SELECT * FROM " + BREAKPOINT_TABLE_NAME, null);
            while (breakpointCursor.goToNextRow()) {
                breakpointInfoRows.add(new BreakpointInfoRow(breakpointCursor));
            }
            blockCursor = db.querySql("SELECT * FROM " + BLOCK_TABLE_NAME, null);
            while (blockCursor.goToNextRow()) {
                blockInfoRows.add(new BlockInfoRow(blockCursor));
            }
        } finally {
            if (breakpointCursor != null) breakpointCursor.close();
            if (blockCursor != null) blockCursor.close();
        }

        final PlainArray<BreakpointInfo> breakpointInfoMap = new PlainArray<>();

        for (BreakpointInfoRow infoRow : breakpointInfoRows) {
            final BreakpointInfo info = infoRow.toInfo();
            final Iterator<BlockInfoRow> blockIt = blockInfoRows.iterator();
            while (blockIt.hasNext()) {
                final BlockInfoRow blockInfoRow = blockIt.next();
                if (blockInfoRow.getBreakpointId() == info.id) {
                    info.addBlock(blockInfoRow.toInfo());
                    blockIt.remove();
                }
            }
            breakpointInfoMap.put(info.id, info);
        }

        return breakpointInfoMap;
    }

    public HashMap<String, String> loadResponseFilenameToMap() {
        ResultSet cursor = null;
        final HashMap<String, String> urlFilenameMap = new HashMap<>();
        final RdbStore db = getWritableDatabase();
        try {
            cursor = db.querySql("SELECT * FROM " + RESPONSE_FILENAME_TABLE_NAME, null);
            while (cursor.goToNextRow()) {
                final String url = cursor.getString(cursor.getColumnIndexForName(URL));
                final String filename = cursor.getString(cursor.getColumnIndexForName(FILENAME));
                urlFilenameMap.put(url, filename);
            }
        } finally {
            if (cursor != null) cursor.close();
        }

        return urlFilenameMap;
    }

    public void updateFilename(@NonNull String url, @NonNull String filename) {
        final RdbStore db = getWritableDatabase();
        ValuesBucket values = new ValuesBucket(2);
        values.putString(URL, url);
        values.putString(FILENAME, filename);

        ResultSet c = null;
        synchronized (url.intern()) {
            try {
                final String query = "SELECT " + FILENAME + " FROM " + RESPONSE_FILENAME_TABLE_NAME
                        + " WHERE " + URL + " = ?";
                c = db.querySql(query, new String[]{url});
                if (c.goToFirstRow()) {
                    // exist
                    if (!filename.equals(c.getString(c.getColumnIndexForName(FILENAME)))) {
                        // replace if not equal
                        db.replace(RESPONSE_FILENAME_TABLE_NAME, values);
                    }
                } else {
                    // insert
                    db.insert(RESPONSE_FILENAME_TABLE_NAME, values);
                }
            } finally {
                if (c != null) c.close();
            }
        }
    }

    public void insert(@NonNull BreakpointInfo info) throws IOException {
        final int blockCount = info.getBlockCount();
        final RdbStore db = getWritableDatabase();
        for (int i = 0; i < blockCount; i++) {
            final BlockInfo blockInfo = info.getBlock(i);
            if (db.insert(BLOCK_TABLE_NAME, toValues(info.id, i, blockInfo)) == -1) {
                throw new SQLiteException("insert block " + blockInfo + " failed!");
            }
        }
        final long result = db.insert(BREAKPOINT_TABLE_NAME,
                toValues(info));
        if (result == -1) throw new SQLiteException("insert info " + info + " failed!");

    }

    public void updateBlockIncrease(@NonNull BreakpointInfo info, int blockIndex,
                                    long newCurrentOffset) {
        final ValuesBucket values = new ValuesBucket();
        RawRdbPredicates updateBlockIncreasePredicates = new RawRdbPredicates(BLOCK_TABLE_NAME, HOST_ID + " = ? AND " + BLOCK_INDEX + " = ?",
                new String[]{Integer.toString(info.id), Integer.toString(blockIndex)});
        values.putLong(CURRENT_OFFSET, newCurrentOffset);
        getWritableDatabase().update(values, updateBlockIncreasePredicates);
    }

    public void updateInfo(@NonNull BreakpointInfo info) throws IOException {
        final RdbStore db = getWritableDatabase();
        ResultSet cursor = null;
        db.beginTransaction();
        try {
            cursor = getWritableDatabase().querySql(
                    "SELECT " + ID + " FROM " + BREAKPOINT_TABLE_NAME + " WHERE " + ID + " ="
                            + info.id + " LIMIT 1",
                    null);
            if (!cursor.goToNextRow()) return; // not exist

            // update
            removeInfo(info.id);
            insert(info);

//            db.setTransactionSuccessful();
            db.markAsCommit();
        } finally {
            if (cursor != null) cursor.close();
            db.endTransaction();
        }
    }

    public void removeInfo(int id) {
        RawRdbPredicates removeInfoPredicates = new RawRdbPredicates(BREAKPOINT_TABLE_NAME, ID + " = ?",
                new String[]{String.valueOf(id)});

        getWritableDatabase().delete(removeInfoPredicates);
        removeBlock(id);
    }

    public void removeBlock(int breakpointId) {
        RawRdbPredicates removeBlockPredicates = new RawRdbPredicates(BLOCK_TABLE_NAME, HOST_ID + " = ?",
                new String[]{String.valueOf(breakpointId)});
        getWritableDatabase().delete(removeBlockPredicates);
    }

    private static ValuesBucket toValues(@NonNull BreakpointInfo info) {
        final ValuesBucket values = new ValuesBucket();
        values.putInteger(ID, info.id);
        values.putString(URL, info.getUrl());
        values.putString(ETAG, info.getEtag());
        values.putString(PARENT_PATH, info.parentFile.getAbsolutePath());
        values.putString(FILENAME, info.getFilename());
        values.putInteger(TASK_ONLY_PARENT_PATH, info.isTaskOnlyProvidedParentPath() ? 1 : 0);
        values.putInteger(CHUNKED, info.isChunked() ? 1 : 0);

        return values;
    }

    private static ValuesBucket toValues(int breakpointId, int index, @NonNull BlockInfo info) {
        final ValuesBucket values = new ValuesBucket();
        values.putInteger(HOST_ID, breakpointId);
        values.putInteger(BLOCK_INDEX, index);
        values.putLong(START_OFFSET, info.getStartOffset());
        values.putLong(CONTENT_LENGTH, info.getContentLength());
        values.putLong(CURRENT_OFFSET, info.getCurrentOffset());
        return values;
    }
}
